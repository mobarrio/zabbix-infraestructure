# **Zabbix de 0 a 100 - Update 08/2023**

## 20/12/2020 - NOTA IMPORTANTE
```
   A día de hoy la ultima versión LTS de Zabbix es la versión 5.0, que si esta soportada en CentOS 7 y en la cual se basa 
   todo el curso. Si queréis probar las ultimas funcionalidades de Zabbix, tendréis que instalar CentOS 8 (No recomendado) 
   Oracle Linux 8 o alguna versión de Ubuntu o Debian entre otras.

   El motivo de esta aclaración es que dados los cambios recientes que comunico Red Hat sobre descontinuar CentOS 8 a los 
   que se suma que Zabbix no liberara los nuevos releases sobre CentOS 7 os dejo unas alternativas con Oracle Linux 8 que 
   es totalmente compatible con todo el material del curso.
 
   En estos momento la ultima release de Zabbix es la 5.2 solo soportada en CentOS 8 que como dije antes se puede instalar 
   para probar pero no esta  recomendado para producción. Por este motivo os dejo algunas  alternativas  para desplegar la
   infraestructura con Oracle Linux 8 y otra para los que les gusta Ubuntu Server 20.04.
```

## Contenido
```
   Se dejan 5 versiones diferentes para poder desplegar la infraestructura. Se recomienda utilizar la ultima disponible a la fecha del clonado.

 ├── README.txt
 ├── centos7                         Es la version en la que esta generada la documentacion.
 ├── centos8                         Es la nueva version de centos que se descontinuara en Diciembre del 2021 pero para la cual Zabbix esta sacando las releasesi superiores a 5.2.
 ├── ol8                             Oracle Linux 8 es compatible en un 99% con la documentacion del curso.
 ├── ol9                             Oracle Linux 9 es compatible en un 99% con la documentacion del curso.
 └── ubuntu                          Ubuntu Server 20.04 que no es compatible con toda la documentacion en lo que respecta a la instalacion de la paqueteria, pero es un reto interesante de realizar.
```

## Estructura de ficheros
```
├── README.txt                       Esta documentacion.
├── ol9                              Oracle Linux 9.
|   ├── provisioning.sh              Aprovisionamiento de la maquinas Cliente y Proxy.
|   ├── ResponseTimeCURL.sh          Script que utilizaremos en algun ejemplo.
|   ├── update-motd                  Muestra una pantalla de inicio personalizada.
|   ├── Vagrantfile                  Despliegue de la Infra con Vagrant + VirtualBox.
|   └── Vagrant-Help.txt             Ejemplos utiles de comandos de Vagrant.
├───scripts                          External scripts
└───templates                        Templates de ejemplo

```
# Requerimientos minimos
**- El entorno se desplegara con maquinas virtuales gestionadas con VirtualBox y Vagrant**
```
 - VirtualBox Versión 7.0.10     Download: https://www.virtualbox.org/
 - Vagrant Versión 2.3.7         Download: https://www.vagrantup.com/downloads
 - Git                           For Windows: https://git-scm.com/download/win
```

# Despliegue de la Infraestructura
## En Linux desde un BASH

```
# cd
# git clone https://mobarrio@bitbucket.org/mobarrio/zabbix-infraestructure.git
# cd zabbix-infraestructure
# cd ol9
# vagrant up

... Esperamos a que despliegue las maquinas

# vagrant ssh zbxsrv01         NOTA: La password del usuario de root es zabbix
```
## En Windows desde un CMD

```
C:\> cd \
C:\> git config --global core.autocrlf false
C:\> git clone https://mobarrio@bitbucket.org/mobarrio/zabbix-infraestructure.git
C:\> cd zabbix-infraestructure
C:\zabbix-infraestructure> cd ol9
C:\zabbix-infraestructure\ol9> vagrant up

... Esperamos a que despliegue las maquinas

C:\zabbix-infraestructure\ol9> vagrant ssh zbxsrv01         NOTA: La password del usuario de root es zabbix
```